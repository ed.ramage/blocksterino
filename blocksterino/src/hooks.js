import {useEffect, useRef} from 'react';

export const useCanv = draw => {
    const canvasRef = useRef(null);

    return canvasRef;
};
