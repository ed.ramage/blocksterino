import React, { useReducer, useMemo } from 'react';
import  mockRoom  from './mockRoom.json'
import { descriptions } from 'jest-config';

const reducer = state => state;


const Room = () => {
    const [roomState, dispatch] = useReducer(reducer, mockRoom);

    return (
      <div>
          <h1>Here we are in {roomState?.name}</h1>
    
      </div>

    )    
};

export default Room;

