import React, {useEffect} from 'react';
import { useCanv } from './Hooks';
const Main = () => {
    const canvasRef = useCanv();

    let draw = ctx => {
        ctx.fillStyle = 'red'
        ctx.fillRect(0,0,100,100);
    }

    useEffect(() => {
        const ctx = canvasRef.current.getContext('2d');
        let animationFrameId = requestAnimationFrame(render);
        function render() {
            animationFrameId = requestAnimationFrame(render);
            draw(ctx);
        }

        return () => cancelAnimationFrame(animationFrameId);
    }, [draw]);


    const handleClick = () => {
        draw = ctx => {
        ctx.fillStyle = 'blue'
        ctx.fillRect(0,0,100,100);
        }
    }

    return (
      <>
      <canvas ref={canvasRef} width="1000" height="1000" />
      <button style={{position: 'absolute', left:'10px', top: '10px'}} onClick={handleClick}>test</button>
      </>
      )
};

export default Main;
